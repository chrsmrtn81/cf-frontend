import "../src/assets/scss/custom.scss";
import React , { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { GlobalContext, GlobalSettings } from "./components/Contexts";
import axios from "axios";
import { isObjectEmpty } from "./helpers/functions";

// Header & Footer
import Header from "./components/Header";
import Navigation from "./components/Navigation";

// Pages
import Property from "./components/Pages/Property";
import Properties from "./components/Pages/Properties";
import Commercial from "./components/Pages/Commercial";
import Static from "./components/Pages/Static";

class App extends Component {  
  constructor(props) {
		super(props);
		this.state = {
      apiURL: "http://192.168.0.8:1337/",
      feedURL: "http://192.168.0.8:8080/json",
      siteSettings: ""
		};
  }
  
  async componentDidMount() {
		this._getSiteSettings();
	}
	
	_getSiteSettings = () => {
    axios
    .get(this.state.apiURL + "site-settings")
		.then((response) => {
			this.setState({ siteSettings: response.data });
		})
		.catch((error) => {
			console.log(error);
		});
	}

  render(){

    console.log("Global Settings:", GlobalSettings)

    if(isObjectEmpty(this.state.siteSettings)){
      return null
    } else {
      return (
        <div className="App">
          <GlobalContext.Provider value={this.state}>
            <Router>
              <Route component={Navigation} />
              <div id="main">
                <Route component={Header} />
                <Switch>
                  <Route path="/property/:slug/" component={Property} />
                  <Route path="/for-sale/" component={Properties} />
                  <Route path="/for-rent/" component={Properties} />
                  <Route path="/commercial/" component={Commercial} />
                  <Route path="/:slug/" component={Static} />
                  <Route path="/" component={Static} />
                </Switch>
              </div>
            </Router>
          </GlobalContext.Provider>
        </div>
      )
    }
  }
}

export default App;
