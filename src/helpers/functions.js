// Check if object is empty
export function isObjectEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}

// Return unique array values
export function uniqueArrayValues(array, index){
  return [...new Set(array.map(item => item[index]))];
}

// Parse date and remove time from string
export function parseDate(dateString){
  var d = dateString;
  d = d.split(' ')[0];
  d = d = d.split('/');
  return Date.parse(d[1] + '/' + d[0] + '/' + d[2]);
}

// Removes time from datetime string
export function dateOnlyString(dateString){
  var d = dateString;
  d = d.split(' ')[0];
  return d;
}

// Truncate text and add elipses 
export function truncateString(string, num) {
  if (string.length <= num) {
    return string
  }
  return string.slice(0, num) + '...'
}