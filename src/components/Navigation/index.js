import React, { Component } from "react";
import { isObjectEmpty } from "../../helpers/functions";
import axios from "axios";
import { Link } from "react-router-dom";
import { GlobalContext } from '../Contexts';
import { navigation__closeNav } from './helpers';

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      main_menu: {},
    };
  }

  static contextType = GlobalContext;

  async componentDidMount() {
    this._getMainMenu();
  }

  async componentDidUpdate(prevProps, prevState){
    if(prevProps.location.pathname !== this.props.location.pathname){
      navigation__closeNav();
    }
  }

  _getMainMenu() {
    axios
      .get(this.context.apiURL + "menus?slug=main-menu")
      .then((response) => {
        this.setState({ main_menu: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {

    if (isObjectEmpty(this.state.main_menu)) {
      return null;
    } else {

      var nav = this.state.main_menu[0].menu_item.map((item, i) => {

        if (item.pages[0].slug === "home-page" && item.active === true) {   
          return (

            <li key={i} onClick={navigation__closeNav}> 
              <Link to="/">{item.title}</Link>
            </li>
            
          );
        } else if (item.pages.length > 1 && item.pages[0].slug !== "home-page" && item.active === true) {
          var subNav = item.pages.map((subItem, x) => {
            return (
              <li key={x} onClick={navigation__closeNav}> 
                <Link to={`/${subItem.slug}`}>{subItem.title}</Link>   
              </li>
            );
          });
          return (
            <li key={i}>
              {item.title}
              <ul className="pl-0">{subNav}</ul>
            </li>
          );
        } else if (item.pages[0].slug !== "home-page" && item.active === true) {
          return (
            <li key={i} onClick={navigation__closeNav}>
              <Link to={`/${item.pages[0].slug}`}>{item.title}</Link>
            </li>
          );
        } else {
          return null;
        }
      });
    
    }

    return (
      <div id="sideNav" className="sideNav">
          <span className="sideNav__closeBtn" onClick={navigation__closeNav}>&times;</span>
          <ul className="pl-0">{nav}</ul>
      </div>
    );
  }
}

export default Navigation;
