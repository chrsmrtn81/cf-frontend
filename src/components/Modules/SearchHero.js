import React, { Component } from "react";
import axios from "axios";
import { isObjectEmpty, uniqueArrayValues } from "../../helpers/functions";
import { Locations, DefaultStatus } from "../Elements/Filters";
import { Container, Row, Col } from "react-bootstrap";
import { FaChevronDown } from "react-icons/fa";
import { GlobalContext } from '../Contexts';

class SearchHero extends Component {
  constructor(props) {
    super(props);
    this.state = {
      saleProperties: {},
      rentProperties: {},
      filterOptions: { minBedrooms: "", maxBedrooms: "", minPrice: "", maxPrice: "", status: "", sortBy: "", propertyType: "",location: "" },
      filterValues: { minBedrooms: "", maxBedrooms: "", minPrice: "", maxPrice: "", status: "", sortBy: "", propertyType: "",location: "" },
      defaultStatus: "sale",
      searchPath: "",
    };
  }

  static contextType = GlobalContext;

  async componentDidMount() {
    axios
      .get(this.context.feedURL)
      .then((response) => {
        this._setProperties(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  _setProperties = (response) => {
    var getSaleProperties = response.data.agency.branches.branch.properties.property.filter(
      (item) => {
        return item.priority === "On Market" || item.priority === "Sold STC";
      }
    );

    var getRentProperties = response.data.agency.branches.branch.properties.property.filter(
      (item) => {
        return (
          item.priority === "Available to Let" || item.priority === "Let STC"
        );
      }
    );

    this.setState(
      { saleProperties: getSaleProperties, rentProperties: getRentProperties },
      () => {
        this._setFilterOptionsAndValues();
      }
    );
  };

  _setFilterOptionsAndValues = () => {

    var properties;
    if (this.state.defaultStatus === "sale") {
      properties = this.state.saleProperties;
    } else if (this.state.defaultStatus === "rent") {
      properties = this.state.rentProperties;
    }

    // Set filter options and value for sale/rent properties
    Object.keys(this.state.filterOptions).forEach((item) => {
      // Bedroom filter
      if (item === "minBedrooms" || item === "maxBedrooms") {

        var uniqueValues = uniqueArrayValues(properties, "bedrooms");
        var numberOfBeds = [];
        for (var i = 1; i <= Math.max(...uniqueValues); i++) {
            numberOfBeds.push(i);
        }

        // Set bedroom filter options and values
        this.setState((prevState) => ({
            filterOptions: { 
            ...prevState.filterOptions,
            [item]: numberOfBeds,
            },
            filterValues: {
            ...prevState.filterValues,
            [item]: "",
            },
        }));

      // Sort by filter
      } else if (item === "sortBy") {

        // Set sort by filter options and values
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: "priceDesc",
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: "priceDesc",
          },
        }));

      // Property type filter
      } else if (item === "propertyType") {
        this.setState((prevState) => ({
          // Set property type filter options and values
          filterOptions: {
            ...prevState.filterOptions,
            [item]: uniqueArrayValues(properties, "property_type"),
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: uniqueArrayValues(properties, "property_type"),
          },
        }));

      // Location filter
      } else if (item === "location") {
        // Set location filter options and values
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: uniqueArrayValues(properties, "town"),
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: uniqueArrayValues(properties, "town"),
          },
        }));

      // Status filter
      } else if (item === "status") {
        var statusOptions;
        var statusValues;
        if (this.state.defaultStatus === "sale") {
          statusOptions = ["On Market", "Sold STC"];
          statusValues = ["On Market", "Sold STC"];
        } else if (this.state.defaultStatus === "rent") {
          statusOptions = ["Available to Let", "Let STC"];
          statusValues = ["Available to Let", "Let STC"];
        }

        // Set status filter options and values
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: statusOptions,
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: statusValues,
          },
        }));

      // Price filter
      } else if (item === "minPrice" || item === "maxPrice") {
        var getPrices = uniqueArrayValues(properties, "numeric_price");
        var getMaxPrice = Math.max(...getPrices);
        var getMinPrice = Math.min(...getPrices);

        var roundUp;
        var increment;
        if (this.state.defaultStatus === "sale") {
          roundUp = 100000;
          increment = 50000;
        } else if (this.state.defaultStatus === "rent") {
          roundUp = 200;
          increment = 200;
        }

        var priceSteps = [];
        priceSteps.push(getMinPrice);
        var roundMinPrice = Math.ceil(getMinPrice / roundUp) * roundUp;
        for (
          roundMinPrice;
          roundMinPrice < getMaxPrice;
          roundMinPrice = roundMinPrice + increment
        ) {
          priceSteps.push(roundMinPrice);
        }
        priceSteps.push(getMaxPrice);

        // Set price filter options and values
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: priceSteps,
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: "",
          },
        }));
      }
    });

  };

  _updateFilterValueArray = (value, filterName) => {
    var index;
    var arrayFilter = this.state.filterValues[filterName];

    if (arrayFilter.indexOf(value) === -1) {
      index = arrayFilter.indexOf(value);
      arrayFilter.push(value);

      this._updateFilterValues(arrayFilter, filterName);
    } else {
      index = arrayFilter.indexOf(value);
      arrayFilter.splice(index, 1);

      this._updateFilterValues(arrayFilter, filterName);
    }
  };

  _updateFilterValues = (value, filterName) => {
    
    this.setState(
      (prevState) => ({
        filterValues: {
          ...prevState.filterValues,
          [filterName]: value,
        },
      }),
      () => {
        var paramsArray = [];
        var paramsArrayString;
        var i = 0;
        Object.keys(this.state.filterValues).forEach((item) => {
          var operator;
          i === 0 ? (operator = "?") : (operator = "&");
          paramsArray.push(
            operator +
              item +
              "=" +
              encodeURIComponent(this.state.filterValues[item])
          );
          i++;
        });

        paramsArrayString = paramsArray.toString().split(",").join("");

        this.setState({
          searchPath: paramsArrayString,
        });
      }
    );
  };

  _updateDefaultStatus = (value, filterName) => {
    this.setState({ 
      [filterName]: value, 
      searchPath: ''
    }, () => {
      this._setFilterOptionsAndValues();
    });
  };

  _goToSearch = () => {
    var pathName;
    if (this.state.defaultStatus === "sale") {
      pathName = "/for-sale";
    } else if (this.state.defaultStatus === "rent") {
      pathName = "/for-rent";
    }
    this.props.props.history.push({
      pathname: pathName,
      search: this.state.searchPath,
    });
  };

  render() {
    if (isObjectEmpty(this.state)) {
      return;
    } else {

      var locationDropdownLabel
      var locations = this.state.filterValues.location
      var numberOfLocationValues = this.state.filterValues.location.length
      var numberOfLocationOptions = this.state.filterOptions.location.length
      if (locations !== ''){
        if (numberOfLocationValues === 0){
            locationDropdownLabel = numberOfLocationValues + ' areas'
        }else if (numberOfLocationValues <= 2 && numberOfLocationValues > 0){
            locationDropdownLabel = locations.join(' and ')
        } else if (numberOfLocationValues > 2 && numberOfLocationValues !== numberOfLocationOptions){
            locationDropdownLabel = numberOfLocationValues + ' areas'
        } else if (numberOfLocationValues === numberOfLocationOptions){
            locationDropdownLabel = 'all areas'
        }
      }

      return (
        <div id="search-hero" className="w-100 search-hero__container">
          <div className="search-hero__gradient"></div>

          <Container className="d-flex h-100">
            <Row className="search-hero__search m-auto p-3">

              {/* Hero title */}
              <Col xs={12} className="text-center p-0">
                <h1 className="search-hero__heading m-0 text-white">
                  Find your perfect property
                </h1>
              </Col>
              {/* Hero links */}
              <Col xs={12} className="my-4 p-0 text-center">
                  <ul className="d-inline-flex text-white p-0 search-hero__menu" >
                      <li className="px-2 px-md-5 search-hero__menu-item">
												<span className="active">Property Search</span>
											</li>
                      <li className="px-2 px-md-5 search-hero__menu-item">
                        <span className="hover">Book a Valuation</span>
											</li>
                  </ul>
              </Col>
              {/* Quickearch options */}
              <Col xs={12} className="p-0">
                <form id="search-hero__search-form">
                  <div className="d-flex mx-auto">
                    
                    <Container className="p-0">
                      <Row>

                        {/* Property status dropdown */}
                        <Col md={5} lg={5} className="mb-3 mb-lg-0">
                          <div className="search-hero__dropdown d-flex p-3">
                            <div className="my-auto mr-auto">
                              <span>
                                Properties <strong>for {this.state.defaultStatus}</strong>
                              </span>
                            </div>
                            <span className="pl-3 search-hero__dropdown-chevron">
                              <FaChevronDown />
                            </span>
                            <div className="search-hero__status-dropdown">
                              <DefaultStatus
                                onChange={this._updateDefaultStatus}
                                name={"defaultStatus"}
                                value={this.state.defaultStatus}
                              />
                            </div>
                          </div>
                        </Col>

                        {/* Property location dropdown */}
                        <Col md={7} lg={5} className="mb-3 mb-lg-0">
                          <div className="search-hero__dropdown d-flex p-3">
                            <div className="my-auto mr-auto">
                              <span>
                                in <strong>{locationDropdownLabel}</strong>
                              </span>
                            </div>
                            <span className="pl-3 search-hero__dropdown-chevron">
                              <FaChevronDown />
                            </span>
                            <div className="search-hero__location-dropdown">
                              <Locations
                                onChange={this._updateFilterValueArray}
                                name={"location"}
                                options={this.state.filterOptions.location}
                                value={this.state.filterValues.location}
                              />
                            </div>
                          </div>
                        </Col>

                        {/* Search button */}
                        <Col sm={12} lg={2}>
                          <button className="btn btn-primary w-100 h-100 p-3 p-lg-0" type="button" onClick={this._goToSearch} >
                            search
                          </button>
                        </Col>

                      </Row>
                    </Container>

                  </div>
                </form>
              </Col>
             
            </Row>
          </Container>

        </div>
      );
    }
  }
}

export default SearchHero;
