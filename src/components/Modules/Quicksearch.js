import React, { Component } from "react";
import axios from "axios";
import { isObjectEmpty, uniqueArrayValues } from "../../helpers/functions";
import { Bedrooms, Prices, SortBy, PropertyTypes, Locations, Status, DefaultStatus } from "../Elements/Filters";
import { GlobalContext } from '../Contexts'

class Quicksearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      saleProperties: {},
      rentProperties: {},
      filterOptions: { minBedrooms: "", maxBedrooms: "", minPrice: "", maxPrice: "", status: "", sortBy: "", propertyType: "", location: "" },
      filterValues: { minBedrooms: "", maxBedrooms: "", minPrice: "", maxPrice: "", status: "", sortBy: "", propertyType: "", location: "" },
      defaultStatus: "sale",
      searchPath: ""
    };
  }

  static contextType = GlobalContext;

  async componentDidMount() {
    axios
      .get(this.context.feedURL)
      .then((response) => {

        this._setPropertiesAndFiltersOptions(response)

      })
      .catch((error) => {
        console.log(error);
      });
  }

  _setPropertiesAndFiltersOptions = (response) => {
    var getSaleProperties = response.data.agency.branches.branch.properties.property.filter(
      (item) => {
          return (
            item.priority === "On Market" || item.priority === "Sold STC"
          )}
    )

    var getRentProperties = response.data.agency.branches.branch.properties.property.filter(
      (item) => {
          return (
            item.priority === "Available to Let" || item.priority === "Let STC"
          )}
    )

    this.setState({ saleProperties: getSaleProperties, rentProperties: getRentProperties }, () => {

      this._setFilterOptions()

    });

  }

  _setFilterOptions = () => {

    var properties
    if(this.state.defaultStatus === 'sale'){
      properties = this.state.saleProperties 
    } else if (this.state.defaultStatus === 'rent'){
      properties = this.state.rentProperties 
    }

    // Set filter options and value for sale/rent properties
    Object.keys(this.state.filterOptions).forEach((item) => {

      // Bedroom filter
      if (item === "minBedrooms" || item === "maxBedrooms") {

        var uniqueValues = uniqueArrayValues(properties, "bedrooms");
        var numberOfBeds = [];
        for (var i = 1; i <= Math.max(...uniqueValues); i++) {
          numberOfBeds.push(i);
        }
        
        // Set bedroom filter options and values
        this.setState((prevState) => ({
          filterOptions: {                // object that we want to update
            ...prevState.filterOptions,   // keep all other key-value pairs
            [item]: numberOfBeds          // update the value of specific key
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: ''
          }
        }));

      // Sort by filter
      } else if (item === "sortBy") {

        // Set sort by filter options and values
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: "priceDesc"
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: "priceDesc"
          }
        }));

      // Property type filter
      } else if (item === "propertyType") {

        this.setState((prevState) => ({

          // Set property type filter options and values
          filterOptions: {
            ...prevState.filterOptions,
            [item]: uniqueArrayValues(properties, 'property_type')
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: uniqueArrayValues(properties, 'property_type')
          }
        }));

      // Location filter  
      } else if (item === "location") {

        // Set location filter options and values
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: uniqueArrayValues(properties, "town")
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: uniqueArrayValues(properties, 'town')
          }
        }));

      // Status filter
      } else if (item === "status") {

        var statusOptions;
        var statusValues;
        if (this.state.defaultStatus === "sale") {
          statusOptions = ["On Market", "Sold STC"];
          statusValues = ["On Market", "Sold STC"];
        } else if (this.state.defaultStatus === "rent") {
          statusOptions = ["Available to Let", "Let STC"];
          statusValues = ["Available to Let", "Let STC"];
        }

        // Set status filter options and values
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: statusOptions
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: statusValues
          }
        }));

      // Price filter
      } else if (item === "minPrice" || item === "maxPrice") {
        
        var getPrices = uniqueArrayValues(properties, "numeric_price");
        var getMaxPrice = Math.max(...getPrices);
        var getMinPrice = Math.min(...getPrices);

        var roundUp;
        var increment;
        if (this.state.defaultStatus === "sale") {
          roundUp = 100000;
          increment = 50000;
        } else if (this.state.defaultStatus === "rent") {
          roundUp = 200;
          increment = 200;
        }

        var priceSteps = [];
        priceSteps.push(getMinPrice);
        var roundMinPrice = Math.ceil(getMinPrice / roundUp) * roundUp;
        for (
          roundMinPrice;
          roundMinPrice < getMaxPrice;
          roundMinPrice = roundMinPrice + increment
        ) {
          priceSteps.push(roundMinPrice);
        }
        priceSteps.push(getMaxPrice);

        // Set price filter options and values
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: priceSteps
          },
          filterValues: {
            ...prevState.filterValues,
            [item]: '',
          }
        }));
      }

    });

  }

  _defaultFilters = () => {

    var properties
    var statuses;
    
    if(this.state.defaultStatus === 'sale'){
      properties = this.state.saleProperties 
      statuses = ["On Market", "Sold STC"];
    } else if (this.state.defaultStatus === 'rent'){
      properties = this.state.rentProperties
      statuses = ["Available to Let", "Let STC"];
    }

    var propertyType = uniqueArrayValues(properties, 'property_type')
    var location = uniqueArrayValues(properties, 'town')

    this.setState((prevState) => ({

      filterValues: {
        ...prevState.filterValues,
        propertyType: propertyType,
        location: location,
        status: statuses,
        sortBy: "priceDesc",
        minBedrooms: "",
        maxBedrooms: "",
        minPrice: "",
        maxPrice: "",
      },
      searchPath: ""

    }))

  }

  _arrayFilters = (value, filterName) => {

    var arrayFilter
    var index
    arrayFilter = this.state.filterValues[filterName]

      if(arrayFilter.indexOf(value) === -1){

        index = arrayFilter.indexOf(value);
        arrayFilter.push(value)

        this._updateFilters(arrayFilter, filterName)
        
      } else {
  
        index = arrayFilter.indexOf(value);
        arrayFilter.splice(index, 1)

        this._updateFilters(arrayFilter, filterName)
  
      }
    
  };

  _updateFilters = (value, filterName) => {

    this.setState(
      (prevState) => ({
        filterValues: {
          ...prevState.filterValues,
          [filterName]: value
        },
      }),
        () => {
        
          var paramsArray = [];
          var paramsArrayString
          var i = 0;
          Object.keys(this.state.filterValues).forEach((item) => {
            var operator;
            i === 0 ? (operator = "?") : (operator = "&");
            paramsArray.push(operator + item + "=" + encodeURIComponent(this.state.filterValues[item]));
            i++;
          });
      
          paramsArrayString = paramsArray.toString().split(",").join("");
          
          this.setState({
              searchPath: 

                paramsArrayString
            
          })

        }
      )

  };

  _updateDefaultStatus = (value, filterName) => {
    this.setState({[filterName]: value},
      () => {
        this._setFilterOptions()
      }
    )
  }

  _goToSearch = () => {

    var pathName
    if(this.state.defaultStatus === 'sale'){
      pathName = '/for-sale'
    } else if (this.state.defaultStatus === 'rent'){
      pathName = '/for-rent' 
    }
    this.props.props.history.push({
      pathname: pathName,
      search: this.state.searchPath,
    });

  }

  render() {

    if (isObjectEmpty(this.props)) {

      return null

    } else {


      return (
        <div>
          <form id="quicksearch__search-form">

            <DefaultStatus
              onChange={this._updateDefaultStatus}
              name={"defaultStatus"}
              value={this.state.defaultStatus}
            />

            <button type="button" onClick={this._defaultFilters}>
              Reset
            </button>

            <button type="button" onClick={this._goToSearch}>
              Go
            </button>

            <SortBy
              onChange={this._updateFilters}
              name={"sortBy"}
              options={this.state.filterOptions.sortBy}
              placeHolder={"Sort by"}
              value={this.state.filterValues.sortBy}
            />

            <Bedrooms
              onChange={this._updateFilters}
              name={"minBedrooms"}
              options={this.state.filterOptions.minBedrooms}
              placeHolder={"Min Beds"}
              value={this.state.filterValues.minBedrooms}
            />

            <Bedrooms
              onChange={this._updateFilters}
              name={"maxBedrooms"}
              options={this.state.filterOptions.maxBedrooms}
              placeHolder={"Max Beds"}
              value={this.state.filterValues.maxBedrooms}
            />

            <Prices
              onChange={this._updateFilters}
              name={"minPrice"}
              options={this.state.filterOptions.minPrice}
              placeHolder={"Min Price"}
              value={this.state.filterValues.minPrice}
            />

            <Prices
              onChange={this._updateFilters}
              name={"maxPrice"}
              options={this.state.filterOptions.maxPrice}
              placeHolder={"Max Price"}
              value={this.state.filterValues.maxPrice}
            />

            <PropertyTypes
              onChange={this._arrayFilters}
              name={"propertyType"}
              options={this.state.filterOptions.propertyType}
              value={this.state.filterValues.propertyType}
            />

            <hr></hr>

            <Locations
              onChange={this._arrayFilters}
              name={"location"}
              options={this.state.filterOptions.location}
              value={this.state.filterValues.location}
            />

            <hr></hr>

            <Status
              onChange={this._arrayFilters}
              name={"status"}
              options={this.state.filterOptions.status}
              value={this.state.filterValues.status}
            />

          </form>

          <hr></hr>
        </div>
      );
    }
  }
}

export default Quicksearch;