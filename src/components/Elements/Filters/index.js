import Bedrooms from './Bedrooms';
import Prices from './Prices';
import SortBy from './SortBy';
import PropertyTypes from './PropertyTypes';
import Locations from './Locations';
import Status from './Status';
import DefaultStatus from './DefaultStatus';


export {
    Bedrooms, Prices, SortBy, PropertyTypes, Locations, Status, DefaultStatus
}