import React, { Component } from "react";
import { isObjectEmpty } from "../../../helpers/functions";

class DefaultStatus extends Component {
  render() {

    if (isObjectEmpty(this.props)) {

      return null

    } else {

      return (
        <div>
          <input type="radio" id="sale" name={this.props.name} value="sale" onChange={e => {this.props.onChange(e.target.value, this.props.name)}} checked={this.props.value === 'sale' ? 'checked' : ''} />
          <label htmlFor="sale">For Sale</label><br />
          <input type="radio" id="rent" name={this.props.name} value="rent" onChange={e => {this.props.onChange(e.target.value, this.props.name)}} checked={this.props.value === 'rent' ? 'checked' : ''} />
          <label htmlFor="rent">For Rent</label><br />
        </div>
      );
    }
  }
}

export default DefaultStatus;