import React, { Component } from "react";
import { isObjectEmpty } from "../../../helpers/functions";

class Bedrooms extends Component {
  render() {

    if (isObjectEmpty(this.props.options)) {

      return null

    } else {

      var options = Object.values(this.props.options).map((item, value) => {
        if(item === 0){
          return(
          <option key={value} value={item}>
            Studio
          </option>
          )
        } else {
          return(
          <option key={value} value={item}>
            {item} Bed
          </option>
          )
        }
      });

      return (
          <select name={this.props.name} className={this.props.className} value={this.props.value} onChange={e => {this.props.onChange(e.target.value, this.props.name)}}>
            <option value="" >{this.props.placeHolder}</option>
            {options}
          </select>
      );
    }
  }
}

export default Bedrooms;