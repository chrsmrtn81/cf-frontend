import React, { Component } from "react";
import { isObjectEmpty } from "../../../helpers/functions";

class Prices extends Component {
  render() {

    if (isObjectEmpty(this.props.options)) {

      return null

    } else {

      return (
        <div>

          <select name={this.props.name} className={this.props.className} value={this.props.value} onChange={e => {this.props.onChange(e.target.value, this.props.name)}}>
            <option value="priceDesc" defaultValue={{ value: "priceDesc" }}>Highest Price</option>
            <option value="priceAsc" >Lowest Price</option>
            <option value="dateDesc" >Newest Listed</option>
            <option value="dateAsc" >Oldest Listed</option>
          </select>

        </div>
      );
    }
  }
}

export default Prices;