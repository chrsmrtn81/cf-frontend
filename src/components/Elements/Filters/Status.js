import React, { Component } from "react";
import { isObjectEmpty } from "../../../helpers/functions";

class Status extends Component {
  render() {
    if (isObjectEmpty(this.props)) {
      return null;
    } else {
      var stcStatus = Object.values(this.props.options).filter(
        (item) => item === "Sold STC" || item === "Let STC"
      );
      var boxChecked;
      var options = stcStatus.map((item, value) => {
        if (this.props.value.indexOf(item) !== -1) {
          boxChecked = true;
        } else {
          boxChecked = false;
        }

        return (
          // **Unstyled default checkbox**
          // <div key={value}>
          //   <input id={this.props.name + '-filter__checkbox-' + value} type="checkbox" name={this.props.name} checked={boxChecked} value={item} onChange={e => {this.props.onChange(e.target.value, this.props.name)}} />
          //   <label htmlFor={this.props.name + '-filter__checkbox-' + value}>{'Include ' + item}</label>
          // </div>

          <label
            key={value}
            className={`checkbox-container ${this.props.labelClassName}`}
            htmlFor={this.props.name + "-filter__checkbox-" + value}
          >
            {"Include " + item}
            <input
              id={this.props.name + "-filter__checkbox-" + value}
              className={this.props.inputClassName}
              type="checkbox"
              name={this.props.name}
              checked={boxChecked}
              value={item}
              onChange={(e) => {
                this.props.onChange(e.target.value, this.props.name);
              }}
            />
            <span className="checkmark"></span>
          </label>
        );
      });

      return <div>{options}</div>;
    }
  }
}

export default Status;
