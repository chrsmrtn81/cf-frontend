import React, { Component } from "react";
import { isObjectEmpty } from "../../../helpers/functions";

class Prices extends Component {
  render() {

    if (isObjectEmpty(this.props.options)) {

      return null

    } else {

      var options = Object.values(this.props.options).map((item, key) => {
        return (
          <option key={key} value={item}>
            {new Intl.NumberFormat('en-EN', { maximumSignificantDigits: 3, style: 'currency', currency: 'GBP' }).format(item)}
          </option>
        );
      });

      return (
        <select name={this.props.name} className={this.props.className} value={this.props.value} onChange={e => {this.props.onChange(e.target.value, this.props.name)}}>
          <option value="" >{this.props.placeHolder}</option>
          {options}
        </select>
      );
    }
  }
}

export default Prices;