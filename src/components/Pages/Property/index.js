import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import axios from "axios";
import { isObjectEmpty } from "../../../helpers/functions";
//const ReactMarkdown = require("react-markdown");
import { GlobalContext } from '../../Contexts';

class Property extends Component {
  constructor(props) {
    super(props);
    this.state = {
      property: {},
    };
  }

  static contextType = GlobalContext;

  async componentDidMount() {
    // axios
    //   .get(
    //     this.context.apiURL +
    //       "properties?slug=" +
    //       this.props.match.params.slug
    //   )
    //   .then((response) => {
    //     this.setState({ property: response.data[0] });
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });

    axios
      .get(this.context.feedURL)
      .then((response) => {
        var getProperties = response.data.agency.branches.branch.properties.property;

        var getProperty = getProperties.filter((property) => 
          property.property_reference === Number(this.props.match.params.slug)
        );

        this.setState({ property: getProperty[0] });
      })
      .catch((error) => {
        console.log(error);
      });

  }

  render() {

    if (isObjectEmpty(this.state.property)) {
      return (
        <div>
          <Container>Loading...</Container>
        </div>
      );
    } else {
      return (
        <div>
          <Container>
            <Row>
              <Col className="col-12">
                <h1>
                  {this.state.property.street}, {this.state.property.town}
                </h1>
                <div>{this.state.property.price_text}</div>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
  }
}

export default Property;
