import React, { Component } from "react";
import { SearchHero } from "../../Modules";
import { isObjectEmpty } from "../../../helpers/functions";
// import { Container, Row, Col } from "react-bootstrap";
import axios from "axios";
import { GlobalContext } from '../../Contexts';

class Static extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageData: "",
      homepage: ""
    };
  }

  static contextType = GlobalContext;

  async componentDidMount() {

    this._getPageData()

  }

  async componentDidUpdate(prevProps, prevState){

    if(prevProps.location.pathname !== this.props.location.pathname){
      this._getPageData()
    }

  }


  _getPageData = () => {
    console.log(this.context);
    var slug
    if(window.location.pathname === "/"){
      slug = this.context.siteSettings.homepage.slug;
    } else {
      slug = this.props.match.params.slug;
    }

    axios
      .get(this.context.apiURL + "pages?slug=" + slug)
      .then((response) => {
        return this.setState({ pageData: response.data[0] });
      })
      .catch((error) => {
        console.log(error);
      });
    
  }

  render() {   
    
    if(isObjectEmpty(this.state)){
      return null;
    } else {

      var text
      if(isObjectEmpty(this.state.pageData)){
        return null
      } else {
        Object.values(this.state.pageData.page_contents).map((item) => {
        return (
          text = item.text
          )
        })
      }


      var searchHero
      if(window.location.pathname === "/"){
        searchHero = <SearchHero props={this.props} />
      } else {
        searchHero = null
      }

      return (
        <div id="static-page">
          
          {searchHero}
          {text}



          <div>
            <ul>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
              <li>testing</li>
            </ul>
          </div>

        </div>
      );
    }
  }
}

export default Static;
