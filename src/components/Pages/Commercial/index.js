import React, { Component } from "react";
import { Container } from "react-bootstrap";
import axios from "axios";
import LazyLoad from "react-lazyload";
import { isObjectEmpty, uniqueArrayValues, parseDate, dateOnlyString } from "../../../helpers/functions";
import { SortBy, PropertyTypes, Locations, Status } from "../../Elements/Filters";
import { Link } from "react-router-dom";
import { GlobalContext } from '../../Contexts';

class Commercial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      properties: {},
      filterOptions: { status: "", sortBy: "", propertyType: "", location: "" },
      filterValues: {status: "", sortBy: "", propertyType: "", location: "" },
    };
  }

  static contextType = GlobalContext;

  async componentDidMount() {
    axios
      .get(this.context.feedURL)
      .then((response) => {

        this._setPropertiesAndFiltersOptions(response)

      })
      .catch((error) => {
        console.log(error);
      });
  }

  async componentDidUpdate(prevProps, prevState) {

    if(prevProps.location.pathname !== this.props.location.pathname){

      axios
      .get(this.context.feedURL)
      .then((response) => {

        this._setPropertiesAndFiltersOptions(response)

      })
      .catch((error) => {
        console.log(error);
      });
    }

    if(prevProps.location.search !== this.props.location.search){

      if(this.props.location.search === ''){
          
        this._defaultFilters()
        
      } else {

        Object.keys(this.state.filterValues).forEach((item) => {

          var urlParams
          if(new URLSearchParams(this.props.location.search).get(item) === null){
  
            if (item === 'propertyType' || item === 'location' || item === 'status'){
                urlParams = []
            } else {
                urlParams = ''
            }
            
          } else {
  
            if (item === 'propertyType' || item === 'location' || item === 'status'){
              urlParams = new URLSearchParams(this.props.location.search).get(item).split(',')
            } else {
              urlParams = new URLSearchParams(this.props.location.search).get(item)
            }
  
          }
  
          this.setState((prevState) => ({
            filterValues: {
              ...prevState.filterValues,
              [item]: urlParams,
            },
          }));
  
        });

      }

    }

  }

  _setPropertiesAndFiltersOptions = (response) => {
    var getProperties = response.data.agency.branches.branch.properties.property.filter(
      (item) => {
        return (
          item.department === "Commercial"
        );
      }
    );

    this.setState({ properties: getProperties }, () => {

      this._setFilterOptions()

    });

  }

  _setFilterOptions = () => {

    // Set filter options and value for sale/rent properties
    Object.keys(this.state.filterOptions).forEach((item) => {

      var urlParams

      if (item === "sortBy") {

        // Set sort by filter options
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: "priceDesc",
          },
        }), () => {

          if(new URLSearchParams(this.props.location.search).get(item) === null){
            urlParams = "priceDesc"
          } else {
            urlParams = new URLSearchParams(this.props.location.search).get(item)
          }

          this.setState((prevState) => ({

            // Set sort by filter value
            filterValues: {
              ...prevState.filterValues,
              [item]: urlParams,
            },

          }))

        });

      // Property type filter
      } else if (item === "propertyType") {

        this.setState((prevState) => ({

          // Set property type filter options
          filterOptions: {
            ...prevState.filterOptions,
            [item]: uniqueArrayValues(this.state.properties, 'property_type'),
          },

        }), () => {

            if(new URLSearchParams(this.props.location.search).get(item) === null){
              urlParams = uniqueArrayValues(this.state.properties, 'property_type')
            } else {
              urlParams = new URLSearchParams(this.props.location.search).get(item).split(',')
            }

            this.setState((prevState) => ({

              // Set property type filter value
              filterValues: {
                ...prevState.filterValues,
                [item]: urlParams,
              },

            }))

          }
        );

      // Location filter  
      } else if (item === "location") {

        // Set location filter options
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: uniqueArrayValues(this.state.properties, "town"),
          },
        }), () => {

          if(new URLSearchParams(this.props.location.search).get(item) === null){
            urlParams = uniqueArrayValues(this.state.properties, 'town')
          } else {
            urlParams = new URLSearchParams(this.props.location.search).get(item).split(',')
          }

          this.setState((prevState) => ({

            // Set location filter value
            filterValues: {
              ...prevState.filterValues,
              [item]: urlParams,
            },

          }))

        });

      // Status filter
      } else if (item === "status") {

        var statuses = ["Available to Let", "Let STC", "On Market", "Sold STC"];
        

        // Set status filter options
        this.setState((prevState) => ({
          filterOptions: {
            ...prevState.filterOptions,
            [item]: statuses,
          },
        }), () => {

          var statuses = ["Available to Let", "Let STC", "On Market", "Sold STC"];

          if(new URLSearchParams(this.props.location.search).get(item) === null){
            urlParams = statuses
          } else {
            urlParams = new URLSearchParams(this.props.location.search).get(item).split(',')
          }

          this.setState((prevState) => ({

            // Set status filter value
            filterValues: {
              ...prevState.filterValues,
              [item]: urlParams,
            },

          }))

        });

      // Price filter
      }

    });


  }

  _defaultFilters = () => {

    var propertyType = uniqueArrayValues(this.state.properties, 'property_type')
    var location = uniqueArrayValues(this.state.properties, 'town')
    var statuses = ["Available to Let", "Let STC", "On Market", "Sold STC"];

    this.setState((prevState) => ({

      filterValues: {
        ...prevState.filterValues,
        "propertyType": propertyType,
        "location": location,
        "status": statuses,
        "sortBy": "priceDesc",
      },

    }))

  }

  _arrayFilters = (value, filterName) => {

    var arrayFilter
    var index
    arrayFilter = this.state.filterValues[filterName]

      if(arrayFilter.indexOf(value) === -1){

        index = arrayFilter.indexOf(value);
        arrayFilter.push(value)

        this._updateFilters(arrayFilter, filterName)
        
      } else {
  
        index = arrayFilter.indexOf(value);
        arrayFilter.splice(index, 1)

        this._updateFilters(arrayFilter, filterName)
  
      }
    
  };

  _updateFilters = (value, filterName) => {

    this.setState(
      (prevState) => ({
        filterValues: {
          ...prevState.filterValues,
          [filterName]: value,
        },
      }),
        () => {
        
          var paramsArray = [];
          var i = 0;
          Object.keys(this.state.filterValues).forEach((item) => {
            var operator;
            i === 0 ? (operator = "?") : (operator = "&");
            paramsArray.push(operator + item + "=" + encodeURIComponent(this.state.filterValues[item]));
            i++;
          });
      
          var paramsArrayString = paramsArray.toString().split(",").join("");
      
          this.props.history.push({
            pathname: this.props.location.pathname,
            search: paramsArrayString
          });

        }
      )

  };

  _resetFilters = () => {

    this.props.history.push({
      pathname: this.props.location.pathname,
      search: "",
    });

  }


  render() {
    var properties = this.state.properties;

    if (isObjectEmpty(properties)) {
      return null;
    } else {

      var filterValues = {
        status: this.state.filterValues.status,
        location: this.state.filterValues.location,
        propertyType: this.state.filterValues.propertyType,
        sortBy: this.state.filterValues.sortBy
      }

      var filterProperties = this.state.properties.filter(item =>
        (filterValues.location.includes(item.town)) &&
        (filterValues.status.includes(item.priority)) &&
        (filterValues.propertyType.includes(item.property_type))
      )

      var sortProperties = filterProperties.sort((a, b) => {
        if(filterValues.sortBy === 'priceDesc'){
          return b.numeric_price - a.numeric_price
        } else if (filterValues.sortBy === 'priceAsc'){
          return a.numeric_price - b.numeric_price
        } else if (filterValues.sortBy === 'dateDesc'){
          return parseDate(b.instructedDate) - parseDate(a.instructedDate)
        } else if (filterValues.sortBy === 'dateAsc'){
          return parseDate(a.instructedDate) - parseDate(b.instructedDate)
        } else {
          return b.numeric_price - a.numeric_price
        }
      });

      var propertiesResults = sortProperties.map((item, i) => {
      
        return (
          <li key={i}>
            <LazyLoad height={30}>
              <img
                style={{ height: "30px" }}
                src={item.pictures.picture[0].filename}
                alt=""
              />
            </LazyLoad>
            <Link to={`/property/${item.property_reference}`}>
              <span style={{color: 'red'}}>{dateOnlyString(item.instructedDate)}</span>
              {
                " - " +
                item.advert_heading +
                " - " +
                item.property_type +
                " - " +
                item.price_text +
                " - " +
                item.bedrooms +
                " beds - " +
                item.priority
                }
            </Link>
          </li>
        );
      });

      var countProperties = filterProperties.length
      
      return (
        <Container>

          <form id="properties__search-form">

            <button type="button" onClick={this._resetFilters}>
              Reset
            </button>

            <SortBy
              onChange={this._updateFilters}
              name={"sortBy"}
              options={this.state.filterOptions.sortBy}
              placeHolder={"Sort by"}
              value={this.state.filterValues.sortBy}
            />

            <PropertyTypes
              onChange={this._arrayFilters}
              name={"propertyType"}
              options={this.state.filterOptions.propertyType}
              value={this.state.filterValues.propertyType}
            />

            <hr></hr>

            <Locations
              onChange={this._arrayFilters}
              name={"location"}
              options={this.state.filterOptions.location}
              value={this.state.filterValues.location}
            />

            <hr></hr>

            <Status
              onChange={this._arrayFilters}
              name={"status"}
              options={this.state.filterOptions.status}
              value={this.state.filterValues.status}
            />
            
          </form>

          <hr></hr>

          <h1>{countProperties}</h1>

          <ol>{propertiesResults}</ol>

        </Container>

      );
    }
  }
}

export default Commercial;
