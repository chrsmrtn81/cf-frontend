import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import axios from "axios";
import LazyLoad from "react-lazyload";
import {
  isObjectEmpty,
  uniqueArrayValues,
  parseDate,
  dateOnlyString,
  truncateString,
} from "../../../helpers/functions";
import {
  Bedrooms,
  Prices,
  SortBy,
  PropertyTypes,
  Locations,
  Status,
} from "../../Elements/Filters";
import { Link } from "react-router-dom";
import { GlobalContext } from "../../Contexts";
import { FiList, FiGrid } from "react-icons/fi";
import { FaChevronRight } from "react-icons/fa";
import BathroomIcon from "../../../assets/img/icons/BathroomIcon.svg";
import ReceptionIcon from "../../../assets/img/icons/ReceptionIcon.svg";
import BedroomIcon from "../../../assets/img/icons/BedroomIcon.svg";
import FloorplanIcon from "../../../assets/img/icons/FloorplanIcon.svg";
import CameraIcon from "../../../assets/img/icons/CameraIcon.svg";
import VideoIcon from "../../../assets/img/icons/VideoIcon.svg";

class Properties extends Component {
  constructor(props) {
    super(props);
    this.state = {
      properties: {},
      filterOptions: {
        minBedrooms: "",
        maxBedrooms: "",
        minPrice: "",
        maxPrice: "",
        status: "",
        sortBy: "",
        propertyType: "",
        location: "",
      },
      filterValues: {
        minBedrooms: "",
        maxBedrooms: "",
        minPrice: "",
        maxPrice: "",
        status: "",
        sortBy: "",
        propertyType: "",
        location: "",
      },
      forSalePath: "/for-sale",
      forRentPath: "/for-rent",
    };
  }

  static contextType = GlobalContext;

  async componentDidMount() {
    axios
      .get(this.context.feedURL)
      .then((response) => {
        this._setPropertiesAndFiltersOptions(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      axios
        .get(this.context.feedURL)
        .then((response) => {
          this._setPropertiesAndFiltersOptions(response);
        })
        .catch((error) => {
          console.log(error);
        });
    }

    if (prevProps.location.search !== this.props.location.search) {
      if (this.props.location.search === "") {
        this._defaultFilters();
      } else {
        Object.keys(this.state.filterValues).forEach((item) => {
          var urlParams;
          if (
            new URLSearchParams(this.props.location.search).get(item) === null
          ) {
            if (
              item === "propertyType" ||
              item === "location" ||
              item === "status"
            ) {
              urlParams = [];
            } else {
              urlParams = "";
            }
          } else {
            if (
              item === "propertyType" ||
              item === "location" ||
              item === "status"
            ) {
              urlParams = new URLSearchParams(this.props.location.search)
                .get(item)
                .split(",");
            } else {
              urlParams = new URLSearchParams(this.props.location.search).get(
                item
              );
            }
          }

          this.setState((prevState) => ({
            filterValues: {
              ...prevState.filterValues,
              [item]: urlParams,
            },
          }));
        });
      }
    }
  }

  _setPropertiesAndFiltersOptions = (response) => {
    var getProperties = response.data.agency.branches.branch.properties.property.filter(
      (item) => {
        if (this.props.location.pathname === this.state.forSalePath) {
          return item.department === "Residential Sales";
        } else if (this.props.location.pathname === this.state.forRentPath) {
          return item.department === "Residential Lettings";
        } else {
          return null;
        }
      }
    );

    this.setState({ properties: getProperties }, () => {
      this._setFilterOptions();
    });
  };

  _setFilterOptions = () => {
    // Set filter options and value for sale/rent properties
    Object.keys(this.state.filterOptions).forEach((item) => {
      var urlParams;

      // Bedroom filter
      if (item === "minBedrooms" || item === "maxBedrooms") {
        var uniqueValues = uniqueArrayValues(this.state.properties, "bedrooms");
        var numberOfBeds = [];
        for (var i = 0; i <= Math.max(...uniqueValues); i++) {
          numberOfBeds.push(i);
        }

        // Set filter options
        this.setState(
          (prevState) => ({
            filterOptions: {
              // object that we want to update
              ...prevState.filterOptions, // keep all other key-value pairs
              [item]: numberOfBeds, // update the value of specific key
            },
          }),
          () => {
            if (
              new URLSearchParams(this.props.location.search).get(item) === null
            ) {
              urlParams = "";
            } else {
              urlParams = new URLSearchParams(this.props.location.search).get(
                item
              );
            }

            this.setState((prevState) => ({
              // Set sort by filter value
              filterValues: {
                ...prevState.filterValues,
                [item]: urlParams,
              },
            }));
          }
        );

        // Sort by filter
      } else if (item === "sortBy") {
        // Set sort by filter options
        this.setState(
          (prevState) => ({
            filterOptions: {
              ...prevState.filterOptions,
              [item]: "priceDesc",
            },
          }),
          () => {
            if (
              new URLSearchParams(this.props.location.search).get(item) === null
            ) {
              urlParams = "priceDesc";
            } else {
              urlParams = new URLSearchParams(this.props.location.search).get(
                item
              );
            }

            this.setState((prevState) => ({
              // Set sort by filter value
              filterValues: {
                ...prevState.filterValues,
                [item]: urlParams,
              },
            }));
          }
        );

        // Property type filter
      } else if (item === "propertyType") {
        this.setState(
          (prevState) => ({
            // Set property type filter options
            filterOptions: {
              ...prevState.filterOptions,
              [item]: uniqueArrayValues(this.state.properties, "property_type"),
            },
          }),
          () => {
            if (
              new URLSearchParams(this.props.location.search).get(item) === null
            ) {
              urlParams = uniqueArrayValues(
                this.state.properties,
                "property_type"
              );
            } else {
              urlParams = new URLSearchParams(this.props.location.search)
                .get(item)
                .split(",");
            }

            this.setState((prevState) => ({
              // Set property type filter value
              filterValues: {
                ...prevState.filterValues,
                [item]: urlParams,
              },
            }));
          }
        );

        // Location filter
      } else if (item === "location") {
        // Set location filter options
        this.setState(
          (prevState) => ({
            filterOptions: {
              ...prevState.filterOptions,
              [item]: uniqueArrayValues(this.state.properties, "town"),
            },
          }),
          () => {
            if (
              new URLSearchParams(this.props.location.search).get(item) === null
            ) {
              urlParams = uniqueArrayValues(this.state.properties, "town");
            } else {
              urlParams = new URLSearchParams(this.props.location.search)
                .get(item)
                .split(",");
            }

            this.setState((prevState) => ({
              // Set location filter value
              filterValues: {
                ...prevState.filterValues,
                [item]: urlParams,
              },
            }));
          }
        );

        // Status filter
      } else if (item === "status") {
        var statuses;
        if (this.props.location.pathname === this.state.forSalePath) {
          statuses = ["On Market", "Sold STC"];
        } else if (this.props.location.pathname === this.state.forRentPath) {
          statuses = ["Available to Let", "Let STC"];
        }

        // Set status filter options
        this.setState(
          (prevState) => ({
            filterOptions: {
              ...prevState.filterOptions,
              [item]: statuses,
            },
          }),
          () => {
            if (this.props.location.pathname === this.state.forSalePath) {
              statuses = ["On Market", "Sold STC"];
            } else if (
              this.props.location.pathname === this.state.forRentPath
            ) {
              statuses = ["Available to Let", "Let STC"];
            }

            if (
              new URLSearchParams(this.props.location.search).get(item) === null
            ) {
              urlParams = statuses;
            } else {
              urlParams = new URLSearchParams(this.props.location.search)
                .get(item)
                .split(",");
            }

            this.setState((prevState) => ({
              // Set status filter value
              filterValues: {
                ...prevState.filterValues,
                [item]: urlParams,
              },
            }));
          }
        );

        // Price filter
      } else if (item === "minPrice" || item === "maxPrice") {
        var getPrices = uniqueArrayValues(
          this.state.properties,
          "numeric_price"
        );
        var getMaxPrice = Math.max(...getPrices);
        var getMinPrice = Math.min(...getPrices);

        var roundUp;
        var increment;
        if (this.props.location.pathname === this.state.forSalePath) {
          roundUp = 100000;
          increment = 50000;
        } else if (this.props.location.pathname === this.state.forRentPath) {
          roundUp = 200;
          increment = 200;
        }

        var priceSteps = [];
        priceSteps.push(getMinPrice);
        var roundMinPrice = Math.ceil(getMinPrice / roundUp) * roundUp;
        for (
          roundMinPrice;
          roundMinPrice < getMaxPrice;
          roundMinPrice = roundMinPrice + increment
        ) {
          priceSteps.push(roundMinPrice);
        }
        priceSteps.push(getMaxPrice);

        // Set filter options
        this.setState(
          (prevState) => ({
            filterOptions: {
              ...prevState.filterOptions,
              [item]: priceSteps,
            },
          }),
          () => {
            if (
              new URLSearchParams(this.props.location.search).get(item) === null
            ) {
              urlParams = "";
            } else {
              urlParams = new URLSearchParams(this.props.location.search).get(
                item
              );
            }

            this.setState((prevState) => ({
              // Set sort by filter value
              filterValues: {
                ...prevState.filterValues,
                [item]: urlParams,
              },
            }));
          }
        );
      }
    });
  };

  _defaultFilters = () => {
    var propertyType = uniqueArrayValues(
      this.state.properties,
      "property_type"
    );
    var location = uniqueArrayValues(this.state.properties, "town");
    var statuses;
    if (this.props.location.pathname === this.state.forSalePath) {
      statuses = ["On Market", "Sold STC"];
    } else if (this.props.location.pathname === this.state.forRentPath) {
      statuses = ["Available to Let", "Let STC"];
    }

    this.setState((prevState) => ({
      filterValues: {
        ...prevState.filterValues,
        propertyType: propertyType,
        location: location,
        status: statuses,
        sortBy: "priceDesc",
        minBedrooms: "",
        maxBedrooms: "",
        minPrice: "",
        maxPrice: "",
      },
    }));
  };

  _arrayFilters = (value, filterName) => {
    var arrayFilter;
    var index;
    arrayFilter = this.state.filterValues[filterName];

    if (arrayFilter.indexOf(value) === -1) {
      index = arrayFilter.indexOf(value);
      arrayFilter.push(value);

      this._updateFilters(arrayFilter, filterName);
    } else {
      index = arrayFilter.indexOf(value);
      arrayFilter.splice(index, 1);

      this._updateFilters(arrayFilter, filterName);
    }
  };

  _updateFilters = (value, filterName) => {
    this.setState(
      (prevState) => ({
        filterValues: {
          ...prevState.filterValues,
          [filterName]: value,
        },
      }),
      () => {
        var paramsArray = [];
        var i = 0;
        Object.keys(this.state.filterValues).forEach((item) => {
          var operator;
          i === 0 ? (operator = "?") : (operator = "&");
          paramsArray.push(
            operator +
              item +
              "=" +
              encodeURIComponent(this.state.filterValues[item])
          );
          i++;
        });

        var paramsArrayString = paramsArray.toString().split(",").join("");

        this.props.history.push({
          pathname: this.props.location.pathname,
          search: paramsArrayString,
        });
      }
    );
  };

  _resetFilters = () => {
    this.props.history.push({
      pathname: this.props.location.pathname,
      search: "",
    });
  };

  render() {
    var properties = this.state.properties;

    if (isObjectEmpty(properties)) {
      return null;
    } else {
      var filterValues = {
        minBedrooms: this.state.filterValues.minBedrooms,
        maxBedrooms: this.state.filterValues.maxBedrooms,
        minPrice: this.state.filterValues.minPrice,
        maxPrice: this.state.filterValues.maxPrice,
        status: this.state.filterValues.status,
        location: this.state.filterValues.location,
        propertyType: this.state.filterValues.propertyType,
        sortBy: this.state.filterValues.sortBy,
      };

      var filterProperties = this.state.properties.filter(
        (item) =>
          (filterValues.minBedrooms === ""
            ? item.bedrooms >= 0
            : item.bedrooms >= filterValues.minBedrooms) &&
          (filterValues.maxBedrooms === ""
            ? item.bedrooms >= 0
            : item.bedrooms <= filterValues.maxBedrooms) &&
          (filterValues.minPrice === ""
            ? item.numeric_price > 0
            : item.numeric_price >= filterValues.minPrice) &&
          (filterValues.maxPrice === ""
            ? item.numeric_price > 0
            : item.numeric_price <= filterValues.maxPrice) &&
          filterValues.location.includes(item.town) &&
          filterValues.status.includes(item.priority) &&
          filterValues.propertyType.includes(item.property_type)
      );

      var sortProperties = filterProperties.sort((a, b) => {
        if (filterValues.sortBy === "priceDesc") {
          return b.numeric_price - a.numeric_price;
        } else if (filterValues.sortBy === "priceAsc") {
          return a.numeric_price - b.numeric_price;
        } else if (filterValues.sortBy === "dateDesc") {
          return parseDate(b.instructedDate) - parseDate(a.instructedDate);
        } else if (filterValues.sortBy === "dateAsc") {
          return parseDate(a.instructedDate) - parseDate(b.instructedDate);
        } else {
          return b.numeric_price - a.numeric_price;
        }
      });

      var propertiesResults = sortProperties.map((item, i) => {
        var x;
        var bulletArray = [];
        for (x = 1; x <= 20; x++) {
          var bullet = "bullet" + x;
          if (item[bullet] !== "") {
            bulletArray.push(item[bullet]);
          }
        }

        var featuresBullets = bulletArray.map((item, i) => {
          return (
            <p className="mb-2" key={i}>
              {item}
            </p>
          );
        });

        return (
          <LazyLoad once={item.once} key={i} height={60} offset={[200, 0]}>
            <li className="mb-5 properties__search-card-list-item">
              <Container>
                <Row>
                  <Col md={5} className="px-0 properties__search-card-gallery">
                    <div className="properties__search-card-gallery-icons">
                      <Link to={`/property/${item.property_reference}`}>
                        <img src={FloorplanIcon} alt="" />
                      </Link>
                      <Link to={`/property/${item.property_reference}`}>
                        <img src={VideoIcon} alt="" />
                      </Link>
                      <Link to={`/property/${item.property_reference}`}>
                        <img src={CameraIcon} alt="" />
                        <span>1/10</span>
                      </Link>
                    </div>
                    <img
                      style={{ width: "100%" }}
                      src={item.pictures.picture[0].filename}
                      alt=""
                    />
                  </Col>
                  <Col
                    md={7}
                    className="d-flex bg-white p-4 properties__search-card-details"
                  >
                    <Link to={`/property/${item.property_reference}`}></Link>
                    <div className="my-auto w-100">
                      {item.priority === "Sold STC" ? (
                        <div className="properties__search-card-stc-label">
                          Sold STC
                        </div>
                      ) : null}
                      {item.priority === "Let STC" ? (
                        <div className="properties__search-card-stc-label">
                          Let STC
                        </div>
                      ) : null}
                      <h4>{item.price_text}</h4>
                      <h3>{item.advert_heading}</h3>
                      <div className="mb-3 properties__search-card-added-on">
                        Added on {dateOnlyString(item.instructedDate)}
                      </div>
                      <ul className="properties__search-card-rooms my-4">
                        <li>
                          <img src={BedroomIcon} alt="" />
                          {item.bedrooms > 1
                            ? item.bedrooms + " bedrooms"
                            : item.bedrooms + " bedroom"}
                        </li>
                        <li>
                          <img src={BathroomIcon} alt="" />
                          {item.bathrooms > 1
                            ? item.bathrooms + " bathrooms"
                            : item.bathrooms + " bathroom"}
                        </li>
                        <li>
                          <img src={ReceptionIcon} alt="" />
                          {item.receptions > 1
                            ? item.receptions + " receptions"
                            : item.receptions + " reception"}
                        </li>
                      </ul>
                      <div>{truncateString(item.main_advert, 200)}</div>
                      <div className="properties__search-card-features d-flex flex-wrap align-content-stretch mt-4">
                        {featuresBullets}
                      </div>
                    </div>
                  </Col>
                </Row>
              </Container>
            </li>
          </LazyLoad>
        );
      });

      var countProperties = filterProperties.length;

      return (
        <div>
          <Container
            fluid
            className="properties__filter-container px-lg-5 py-4"
          >
            <form id="properties__search-filters">
              <Row>
                <Col sm={11}>
                  <Row>
                    <Col md={2}>
                      <Bedrooms
                        onChange={this._updateFilters}
                        name={"minBedrooms"}
                        options={this.state.filterOptions.minBedrooms}
                        placeHolder={"Min Beds"}
                        value={this.state.filterValues.minBedrooms}
                        className={"properties__select w-100"}
                      />
                    </Col>
                    <Col md={2}>
                      <Bedrooms
                        onChange={this._updateFilters}
                        name={"maxBedrooms"}
                        options={this.state.filterOptions.maxBedrooms}
                        placeHolder={"Max Beds"}
                        value={this.state.filterValues.maxBedrooms}
                        className={"properties__select w-100"}
                      />
                    </Col>
                    <Col md={2}>
                      <Prices
                        onChange={this._updateFilters}
                        name={"minPrice"}
                        options={this.state.filterOptions.minPrice}
                        placeHolder={"Min Price"}
                        value={this.state.filterValues.minPrice}
                        className={"properties__select w-100"}
                      />
                    </Col>
                    <Col md={2}>
                      <Prices
                        onChange={this._updateFilters}
                        name={"maxPrice"}
                        options={this.state.filterOptions.maxPrice}
                        placeHolder={"Max Price"}
                        value={this.state.filterValues.maxPrice}
                        className={"properties__select w-100"}
                      />
                    </Col>
                    <Col md={2}>
                      <div className="properties__checkbox-dropdown-container d-flex">
                        <div className="my-auto mr-auto">
                          <span>Locations</span>
                        </div>
                        <div className="properties__checkbox-dropdown">
                          <Locations
                            onChange={this._arrayFilters}
                            name={"location"}
                            options={this.state.filterOptions.location}
                            value={this.state.filterValues.location}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col md={2}>
                      <div className="properties__checkbox-dropdown-container d-flex">
                        <div className="my-auto mr-auto">
                          <span>Property Type</span>
                        </div>
                        <div className="properties__checkbox-dropdown">
                          <PropertyTypes
                            onChange={this._arrayFilters}
                            name={"propertyType"}
                            options={this.state.filterOptions.propertyType}
                            value={this.state.filterValues.propertyType}
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col sm={1}>
                  <button
                    className="btn btn-primary w-100 h-100"
                    type="button"
                    onClick={this._resetFilters}
                  >
                    Reset
                  </button>
                </Col>
              </Row>
            </form>
          </Container>

          <Container className="properties__secondary-filter-container py-4">
            <Row>
              <Col xs={12} md={5} className="d-flex order-2 order-md-1">
                <h1 className="my-auto">
                  Residential properties for sale (
                  <strong>{countProperties}</strong>)
                </h1>
              </Col>
              <Col xs={12} md={7} className="order-1 order-md-2">
                <ul className="pl-0 mb-0 text-md-right">
                  <li>
                    <Status
                      onChange={this._arrayFilters}
                      name={"status"}
                      options={this.state.filterOptions.status}
                      value={this.state.filterValues.status}
                      inputClassName={"mb-0"}
                      labelClassName={"mb-0"}
                    />
                  </li>
                  <li>
                    <div>
                      <span>
                        <FiList />
                      </span>
                      List view
                    </div>
                    <div>
                      <span>
                        <FiGrid />
                      </span>
                      Grid view
                    </div>
                  </li>
                  <li>
                    <SortBy
                      onChange={this._updateFilters}
                      name={"sortBy"}
                      options={this.state.filterOptions.sortBy}
                      placeHolder={"Sort by"}
                      value={this.state.filterValues.sortBy}
                      className={"properties__select pl-0"}
                    />
                  </li>
                </ul>
              </Col>
            </Row>
          </Container>

          <Container className="properties__search-card-container">
            <ol>{propertiesResults}</ol>
          </Container>
        </div>
      );
    }
  }
}

export default Properties;
