import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { isObjectEmpty } from "../../helpers/functions";
import axios from "axios";
import logoLight from "../../assets/img/logo-light.png";
import logoDark from "../../assets/img/logo-dark.png";
//import logoWhite from "../../assets/img/logo-white.png";
import { AiOutlineBars, AiOutlineSearch, AiOutlinePhone } from "react-icons/ai";
import { Link } from "react-router-dom";
import { GlobalContext } from '../Contexts';
import { navigation__openNav } from '../Navigation/helpers';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topMenu: "",
      siteSettings: "",
      headerClass: "",
      logoImage: "",
      logoScrollImage: logoDark 
    };
  }

  static contextType = GlobalContext;

  async componentDidMount() {
    this._setHeaderVariant();
    this._getTopMenu();
  }

  async componentDidUpdate(prevProps, prevState){
    if(prevProps.location.pathname !== this.props.location.pathname){
      this._setHeaderVariant();
    }
  }

  _setHeaderVariant = () => {
    if(window.location.pathname === "/"){
      this.setState({headerClass: "header-alt"});
      this.setState({logoImage: logoLight});
    } else {
      this.setState({headerClass: "header"});
      this.setState({logoImage: logoDark});
    }
  }

  _getTopMenu = () => {
    axios
      .get(this.context.apiURL + "menus?slug=top-menu")
      .then((response) => {
        this.setState({ topMenu: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  _changeHeaderOnScroll = () => {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      document.getElementsByClassName(this.state.headerClass)[0].classList.add('header-scroll');
      document.getElementsByClassName(this.state.headerClass + '__logo')[0].classList.add('header-scroll__logo');
      document.getElementsByClassName(this.state.headerClass + '__top-menu')[0].classList.add('header-scroll__top-menu');
      document.getElementsByClassName(this.state.headerClass + '__logo')[0].src = this.state.logoScrollImage;
    } else {
      document.getElementsByClassName(this.state.headerClass)[0].classList.remove('header-scroll');
      document.getElementsByClassName(this.state.headerClass + '__logo')[0].classList.remove('header-scroll__logo');
      document.getElementsByClassName(this.state.headerClass + '__top-menu')[0].classList.remove('header-scroll__top-menu');
      document.getElementsByClassName(this.state.headerClass + '__logo')[0].src = this.state.logoImage;
    }

  }

  render() {

    if (isObjectEmpty(this.state.topMenu)) {
      return null
    } else {

      var topMenu = this.state.topMenu[0].menu_item.map((item, i) => {

        if (item.pages.length > 1 && item.active === true) {
          var subNav = item.pages.map((subItem, x) => {
            return (
              <li key={x} className="text-center">    
                <span><Link to={`/${subItem.slug}`}>{subItem.title}</Link></span>
              </li>
            );
          });
          return (
            <li key={i} className="text-center">
              <span className="hover">{item.title}</span>
              {/* <ul className="pl-0">{subNav}</ul> */}
            </li>
          );
        } else if (item.pages.length === 1 && item.active === true) {
          return (
            <li key={i} className="text-center">
              <span className="hover"><Link to={`/${item.pages[0].slug}`}>{item.title}</Link></span>
            </li>
          );
        } else {
          return null;
        }
      });

      window.onscroll = () => {
        this._changeHeaderOnScroll(this.state.headerClass)
      };

         
      return (
        <div id='header'>
            <Container fluid className={`${this.state.headerClass}`}>
              <Row className="h-100">

                {/* Logo */}
                <Col xs={6} xl={2} className="d-flex">
                  <div className={`d-flex ${this.state.headerClass}__logo-container`}>
                  <Link to="/" className="my-auto">
                    <img className={`${this.state.headerClass}__logo`} src={this.state.logoImage} alt="" />
                  </Link>
                  </div>
                </Col>

                {/* Top menu */}
                <Col xs={0} xl={8} className="d-none d-xl-flex">
                  <div className="m-auto">
                    <ul className={`mb-0 d-inline-flex p-0 ${this.state.headerClass}__top-menu`}>
                      {topMenu}
                    </ul>
                  </div>
                </Col>

                {/* Header icons */}
                <Col xs={6} xl={2} className="my-3">
                  <div className="h-100 d-flex flex-row-reverse">
                    <span className={`my-auto ml-2 d-flex ${this.state.headerClass}__icon-menu`} onClick={navigation__openNav}>< AiOutlineBars /></span>
                    <span className={`my-auto mx-2 d-flex ${this.state.headerClass}__icon-menu`}>< AiOutlineSearch /></span>
                    <span className={`my-auto mx-2 d-flex ${this.state.headerClass}__icon-menu`}>< AiOutlinePhone /></span>
                  </div>
                </Col>

              </Row>
            </Container>
          <div className={`${this.state.headerClass}__height-offset`}></div>
        </div>
      );
    }
  }
}

export default Header;
