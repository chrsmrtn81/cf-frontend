import React from "react";
import axios from "axios";

export const GlobalContext = React.createContext();

export const GlobalSettings = {
  apiURL: "http://192.168.0.8:1337/",
  feedURL: "http://192.168.0.8:8080/json",
};

